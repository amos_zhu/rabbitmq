package com.amos.consumer.receiver;

import com.amos.common.bean.Result;
import com.amos.common.message.MessageData;
import com.amos.common.util.ResultWapper;
import com.amos.consumer.bean.HandleResult;
import com.amos.consumer.command.CallBackCommand;
import com.amos.consumer.handle.AbstractHandler;
import com.amos.consumer.handle.Invoker;
import com.amos.consumer.type.HandleSignalEnum;
import com.amos.consumer.type.MessageTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 指令消息处理
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
@Component
public class OperReceiver extends AbstractReceiver {

    @Autowired
    Invoker invoker;

    @Autowired
    CallBackCommand callBackCommand;

    @Autowired
    @Qualifier("callBackHandler")
    AbstractHandler callbackHandler;

    /**
     * 针对不同的指令进行不同的处理
     *
     * @param messageData
     * @return
     */
    @Override
    public HandleResult exec(MessageData messageData) {

        // 如果是回调指令 则交给回调处理者来处理
        if (MessageTypeEnum.SEND_TYPE_CALLBACK.getCode().equals(messageData.getMsgType())) {

            this.invoker.setCommand(this.callBackCommand.init(this.callbackHandler));
        }

        return this.invoker.execute(messageData);
    }

    /**
     * 指令消息不需要经过验证
     *
     * @return
     */
    @Override
    public Result validate(MessageData messageData) {
        return ResultWapper.success();
    }

    @Override
    public HandleResult handleSuccess(MessageData messageDat) {
        return new HandleResult.CallBack(true).type(HandleSignalEnum.SINGAL_CALLBACK.getCode()).builder();
    }

    @Override
    public HandleResult handleFail(MessageData messageDat) {
        return new HandleResult.CallBack(true).type(HandleSignalEnum.SINGAL_CALLBACK.getCode()).builder();
    }
}
