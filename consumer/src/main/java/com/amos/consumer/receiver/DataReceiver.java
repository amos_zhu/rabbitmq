package com.amos.consumer.receiver;

import com.amos.common.bean.Result;
import com.amos.common.message.MessageData;
import com.amos.common.util.ResultWapper;
import com.amos.consumer.bean.HandleResult;
import com.amos.consumer.type.HandleSignalEnum;
import com.amos.consumer.type.MessageTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 数据消息的接收处理
 * <p/>
 * 这里主要是业务方集成AbstractReceiver 实现自定义的数据处理
 * 一般对于内容型的消息都需要返回给对方我方到底是否成功消费，还是系统或业务出现了异常
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
@Component
public class DataReceiver extends AbstractReceiver {
    private static final Logger logger = LoggerFactory.getLogger(AbstractReceiver.class);

    /**
     * 对内容消息进行处理
     *
     * @param messageData
     * @return
     * @throws Exception
     */
    @Override
    public HandleResult exec(MessageData messageData) throws Exception {
        HandleResult handleResult;

        try {
            // TODO 业务方的处理 并且添加自定义的日志记录

            handleResult = new HandleResult.CallBack(Boolean.TRUE).type(MessageTypeEnum.SEND_TYPE_CALLBACK.getCode()).builder();
        } catch (Exception e) {
            if (logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            handleResult = new HandleResult.CallBack(Boolean.FALSE).type(MessageTypeEnum.SEND_TYPE_CALLBACK.getCode()).msg("接收处理错误:" + e.getMessage()).builder();
        }

        return handleResult;
    }

    /**
     * 是否需要进行好友验证
     *
     * @return
     */
    @Override
    public Result validate(MessageData messageData) {
        // TODO 实现用户自定义校验逻辑
        return ResultWapper.success();
    }

    /**
     * 成功处理
     * 内容需要回调
     *
     * @param messageData
     * @return
     */
    @Override
    public HandleResult handleSuccess(MessageData messageData) {
        // TODO 自定义成功的业务处理
        HandleResult handleResult = new HandleResult.CallBack(true).type(HandleSignalEnum.SINGAL_CALLBACK.getCode()).builder();
        return handleResult;
    }


    /**
     * 失败处理
     *
     * @param messageData
     * @return
     */
    @Override
    public HandleResult handleFail(MessageData messageData) {
        // TODO 自定义失败的业务处理
        HandleResult handleResult = new HandleResult.CallBack(false).type(HandleSignalEnum.SINGAL_CALLBACK.getCode()).builder();
        return handleResult;
    }

}
