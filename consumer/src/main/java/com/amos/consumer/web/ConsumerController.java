package com.amos.consumer.web;

import com.amos.common.bean.MqExchange;
import com.amos.common.bean.MqQueue;
import com.amos.common.bean.Result;
import com.amos.common.declare.AmBindDeclare;
import com.amos.common.declare.AmExchangeDeclare;
import com.amos.common.declare.AmQueueDeclare;
import com.amos.common.type.ExchangeTypeEnum;
import com.amos.common.util.RabbitMQUtils;
import com.amos.common.util.ResultWapper;
import com.amos.consumer.service.RegisterQueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: ConsumerController
 * @Package: com.amos.consumer.web
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 下午 16:53
 * @Version: V1.0
 */
@RestController
@RequestMapping(value = "/consumer")
public class ConsumerController {
    @Autowired
    AmExchangeDeclare amExchangeDeclare;

    @Autowired
    AmQueueDeclare amQueueDeclare;

    @Autowired
    AmBindDeclare amBindDeclare;

    @Autowired
    RegisterQueueService registerQueueService;

    @Autowired
    RabbitMQUtils rabbitMQUtils;

    /**
     * 动态注册交换机
     *
     * @param mqExchage
     * @return
     */
    @PostMapping(value = "/declareDirectExchange")
    public Result declareDirectExchange(@RequestBody MqExchange mqExchage) {
        mqExchage.setType(ExchangeTypeEnum.DIRECT);
        this.amExchangeDeclare.declareExchange(mqExchage);
        return ResultWapper.success();
    }

    /**
     * 动态注册队列
     *
     * @param mqQueue
     * @return
     */
    @PostMapping(value = "/declareQueue")
    public Result declareQueue(@RequestBody MqQueue mqQueue) {
        this.amQueueDeclare.declareQueue(mqQueue);
        return ResultWapper.success();
    }

    /**
     * 判断对应的queue是否存在
     *
     * @param queueName
     * @return
     */
    @GetMapping(value = "/{queueName}")
    public Result isExistQueue(@PathVariable("queueName") String queueName) {
        return ResultWapper.success(this.amQueueDeclare.isQueueExist(queueName));
    }

    /**
     * 队列绑定
     *
     * @param queue
     * @param exchange
     * @return
     */
    @GetMapping(value = "/bindQueue/{queue}")
    public Result queueBind(@PathVariable("queue") String queue,
                            @RequestParam(value = "exchange", required = false) String exchange) {
        if (StringUtils.isEmpty(exchange)) {
            return ResultWapper.success(this.amBindDeclare.queueBind(queue));
        } else {
            return ResultWapper.success(this.amBindDeclare.queueBind(queue, exchange, queue));
        }
    }

    /**
     * 注册队列，并且设置该对应的监听
     *
     * @param queue
     * @return
     */
    @GetMapping(value = "registerQueue")
    public Result listenQueue(@RequestParam("queue") String queue) {
        this.registerQueueService.queue(queue).exchange(MqExchange.DEFAULT_DIRECT_EXCHANGE);
        this.registerQueueService.registerQueue();
        return ResultWapper.success();
    }

    /**
     * 查询所有的交换机
     *
     * @return
     */
    @GetMapping(value = "/listExchanges")
    public Result listExchange() {
        return ResultWapper.success(this.rabbitMQUtils.listExchanges());
    }
}
