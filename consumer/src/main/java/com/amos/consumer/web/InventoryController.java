package com.amos.consumer.web;

import com.amos.common.bean.Result;
import com.amos.common.util.ResultWapper;
import com.amos.consumer.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.consumer.web
 * @ClassName InventoryController
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/8/18 22:18
 * @Version 1.0
 **/
@RestController
@RequestMapping(value = "/inventory")
public class InventoryController {
    @Autowired
    InventoryService inventoryService;

    /**
     * 根据id查询库存相关信息
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/selectById/{id}")
    public Result selectByid(@PathVariable("id") String id) {
        return ResultWapper.success(this.inventoryService.selectById(id));
    }


}
