package com.amos.consumer.web;

import com.amos.common.bean.Result;
import com.amos.common.util.RedisUtils;
import com.amos.common.util.ResultWapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.web
 * @ClassName RedisController
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/8/18 21:06
 * @Version 1.0
 **/
@RequestMapping(value = "/redis")
@RestController
public class RedisController {
    @Autowired
    RedisUtils redisUtils;

    /**
     * 测试redis保存功能
     *
     * @param key
     * @return
     */
    @GetMapping(value = "/save/{key}")
    public Result save(@PathVariable("key") String key) {
        return ResultWapper.success(this.redisUtils.save(key, "测试redis保存"));
    }
}
