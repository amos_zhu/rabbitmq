package com.amos.consumer.bean;

import com.amos.consumer.type.HandleSignalEnum;
import org.springframework.util.StringUtils;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 处理结果
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public class HandleResult {
    /**
     * 是否回调
     */
    private boolean callback;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 内容
     */
    private String content;
    /**
     * 消息
     */
    private String msg;
    /**
     * 是否处理成功
     */
    private boolean result;

    public HandleResult(CallBack callBack) {
        this.callback = callBack.callback;
        this.content = callBack.content;
        this.type = callBack.type;
        this.msg = callBack.msg;
        this.result = callBack.callback;
    }

    public boolean isCallback() {
        return this.callback;
    }

    public void setCallback(boolean callback) {
        this.callback = callback;
    }

    public Integer getType() {
        if (StringUtils.isEmpty(this.type)) {
            this.type = HandleSignalEnum.SINGAL_CALLBACK.getCode();
        }
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getResult() {
        return this.result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public static class CallBack {
        /**
         * 是否回调
         */
        private boolean callback = true;
        /**
         * 回调请求类型
         */
        private Integer type;
        /**
         * 回调内容
         */
        private String content;

        private String msg;
        /**
         * 是否处理成功
         */
        private boolean result;

        public CallBack(boolean result) {
            this.result = result;
        }

        public CallBack type(Integer type) {
            this.type = type;
            return this;
        }

        public CallBack content(String content) {
            this.content = content;
            return this;
        }

        public CallBack callback(boolean callback) {
            this.callback = callback;
            return this;
        }

        public CallBack msg(String msg) {
            this.msg = msg;
            return this;
        }

        public HandleResult builder() {
            return new HandleResult(this);
        }
    }
}
