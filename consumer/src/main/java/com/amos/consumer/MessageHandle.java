package com.amos.consumer;

import com.amos.common.listen.AbstractMessageHandler;
import com.rabbitmq.client.Channel;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: MessageHandle
 * @Package: com.amos.consumer
 * @author: zhuqb
 * @Description: 消息接收处理
 * @date: 2019/7/2 0002 下午 18:38
 * @Version: V1.0
 */
@Component
public class MessageHandle extends AbstractMessageHandler {


    @Override
    public void handleMessage(String message, Channel channel) {
        this.logger.info("进入自定义处理方法中......\n消息为：" + message);
    }
}
