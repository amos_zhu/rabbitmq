package com.amos.consumer.service;

import com.amos.common.listen.AbstractMessageHandler;
import com.amos.common.register.AbstractRegisterQueue;
import com.amos.consumer.MessageHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.service
 * @ClassName RegisterQueueService
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/7/2 22:21
 * @Version 1.0
 **/
@Service
public class RegisterQueueService extends AbstractRegisterQueue {
    @Autowired
    MessageHandle messageHandle;


    @Override
    public AbstractMessageHandler messageHandler() {
        return this.messageHandle;
    }
}
