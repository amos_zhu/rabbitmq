package com.amos.consumer.service;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 对消息进行处理
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public interface Receiver {
    /**
     * 对消息进行处理
     *
     * @param messageData
     * @return
     */
    HandleResult handleMessage(MessageData messageData);
}
