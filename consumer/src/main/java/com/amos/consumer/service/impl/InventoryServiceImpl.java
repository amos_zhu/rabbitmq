package com.amos.consumer.service.impl;

import com.amos.consumer.entity.Inventory;
import com.amos.consumer.mapper.InventoryMapper;
import com.amos.consumer.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.consumer.service.impl
 * @ClassName InventoryServiceImpl
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/8/18 22:16
 * @Version 1.0
 **/
@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    InventoryMapper inventoryMapper;

    /**
     * 根据id查询库存信息
     *
     * @param id
     * @return
     */
    @Override
    public Inventory selectById(String id) {
        return this.inventoryMapper.selectById(id);
    }
}
