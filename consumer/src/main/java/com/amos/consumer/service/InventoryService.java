package com.amos.consumer.service;

import com.amos.consumer.entity.Inventory;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.consumer.service
 * @ClassName InventoryService
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/8/18 22:16
 * @Version 1.0
 **/
public interface InventoryService {
    /**
     * 根据id查询库存信息
     *
     * @param id 库存ID
     * @return
     */
    Inventory selectById(String id);
}
