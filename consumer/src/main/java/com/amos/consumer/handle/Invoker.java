package com.amos.consumer.handle;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;
import com.amos.consumer.command.AbstractCommand;
import org.springframework.stereotype.Component;


/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 命令的调用者
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
@Component
public class Invoker {

    private AbstractCommand command;

    public void setCommand(AbstractCommand command) {
        this.command = command;
    }

    public HandleResult execute(MessageData messageData) {
        return this.command.execute(messageData);
    }
}
