package com.amos.consumer.handle;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 回调消息处理者
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
@Component
public class CallBackHandler extends AbstractHandler {

    /**
     * 修改消息
     *
     * @param data
     * @return
     */
    @Override
    public HandleResult handle(MessageData data) {
        // TODO  自定义业务逻辑处理
        return new HandleResult.CallBack(true).callback(false).msg("处理成功").builder();
    }
}
