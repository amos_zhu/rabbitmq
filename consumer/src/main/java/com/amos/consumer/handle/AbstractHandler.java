package com.amos.consumer.handle;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;


/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 处理消息 抽象类
 * <p/>
 * 业务处理需要继承该基类，实现处理的方法
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public abstract class AbstractHandler {
    /**
     * 自定义处理
     *
     * @param data
     * @return
     */
    public abstract HandleResult handle(MessageData data);
}
