package com.amos.consumer.type;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 消息的类型
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public enum MessageTypeEnum {
    /**
     * 发送内容
     */
    SEND_TYPE_CONTENT(1, "内容"),
    /**
     * 拒绝
     */
    SEND_TYPE_RESEND(-1, "拒绝"),
    /**
     * 回调
     */
    SEND_TYPE_CALLBACK(0, "回调"),
    /**
     * 同意
     */
    SEND_TYPE_AGREE(2, "同意"),
    /**
     * 异常
     */
    SEND_TYPE_ERROR(9, "异常");

    private Integer code;
    private String desc;

    private MessageTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
