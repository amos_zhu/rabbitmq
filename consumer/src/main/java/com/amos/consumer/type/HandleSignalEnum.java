package com.amos.consumer.type;


/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 消息处理信号枚举
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public enum HandleSignalEnum {
    /**
     * 回调
     */
    SINGAL_CALLBACK(0, "回调"),
    /**
     * 内容
     */
    SIGNAL_DATA(6, "内容");

    private Integer code;
    private String desc;

    private HandleSignalEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
