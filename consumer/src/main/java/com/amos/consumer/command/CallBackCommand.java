package com.amos.consumer.command;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;
import com.amos.consumer.handle.AbstractHandler;
import org.springframework.stereotype.Component;


/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 回调函数 消息处理
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
@Component
public class CallBackCommand extends AbstractCommand {
    /**
     * 定义handler来进行命令处理
     */
    private AbstractHandler handler;

    public CallBackCommand(AbstractHandler handler) {
        this.handler = handler;
    }


    public CallBackCommand init(AbstractHandler handler) {
        this.handler = handler;
        return this;
    }

    /**
     * 执行业务处理
     *
     * @param unicomData
     * @return
     */
    @Override
    public HandleResult execute(MessageData unicomData) {
        return this.handler.handle(unicomData);
    }
}
