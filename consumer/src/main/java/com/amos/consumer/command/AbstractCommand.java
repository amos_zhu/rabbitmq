package com.amos.consumer.command;

import com.amos.common.message.MessageData;
import com.amos.consumer.bean.HandleResult;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.send
 * @ClassName SendService
 * @Description 命令抽象类
 * <p/>
 * 把接收消息的类型封装成一个命令 并且交给指定的接收者出处理
 * 方便扩展每个命令的处理
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:11
 * @Version 1.0
 **/
public abstract class AbstractCommand {

    /**
     * 每个命令都必须被处理
     *
     * @param messageData
     * @return
     */
    public abstract HandleResult execute(MessageData messageData);
}
