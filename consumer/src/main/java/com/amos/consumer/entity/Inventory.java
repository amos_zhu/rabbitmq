package com.amos.consumer.entity;

import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.consumer.entity
 * @ClassName Inventory
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/8/18 22:11
 * @Version 1.0
 **/
@Data
public class Inventory {
    private String id;
    private Integer count;

}
