package com.amos.producer.web;

import com.amos.common.bean.MqExchange;
import com.amos.common.bean.Result;
import com.amos.common.declare.AmBindDeclare;
import com.amos.common.declare.AmExchangeDeclare;
import com.amos.common.message.MessageData;
import com.amos.common.type.ExchangeTypeEnum;
import com.amos.common.util.ResultWapper;
import com.amos.producer.send.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: ProducerController
 * @Package: com.amos.producer.web
 * @author: zhuqb
 * @Description: 生产者的Controller
 * @date: 2019/7/2 0002 下午 16:24
 * @Version: V1.0
 */
@RestController
@RequestMapping(value = "/producer")
public class ProducerController {
    @Autowired
    AmExchangeDeclare amExchangeDeclare;
    @Autowired
    AmBindDeclare amBindDeclare;
    @Autowired
    SendService sendService;

    /**
     * 注册队列
     *
     * @param exchange
     * @param type
     * @return
     */
    @GetMapping(value = "/registerExchange")
    public Result declareExchange(@RequestParam("exchange") String exchange,
                                  @RequestParam(value = "type", required = false) Integer type) {
        MqExchange mqExchange = new MqExchange().name(exchange);

        if (StringUtils.isEmpty(type)) {
            mqExchange.type(ExchangeTypeEnum.DIRECT);
        } else if (Integer.valueOf(1).equals(type)) {
            mqExchange.type(ExchangeTypeEnum.FANOUT);
        } else if (Integer.valueOf(2).equals(type)) {
            mqExchange.type(ExchangeTypeEnum.TOPIC);
        } else if (Integer.valueOf(3).equals(type)) {
            mqExchange.type(ExchangeTypeEnum.HEADERS);
        } else {
            return ResultWapper.error("类型不对");
        }
        return ResultWapper.success(this.amExchangeDeclare.declareExchange(mqExchange));
    }


    /**
     * 消息发送
     *
     * @param messageData
     * @return
     */
    @PostMapping(value = "/send")
    public Result send(@RequestBody MessageData messageData) {
        this.sendService.send(MqExchange.DEFAULT_DIRECT_EXCHANGE, messageData.getReceiver(), messageData, null, "");
        return ResultWapper.success();
    }

}
