package com.amos.producer.send;

import com.amos.common.send.AbstractSendService;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Service;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: SendService
 * @Package: com.amos.consumer
 * @author: zhuqb
 * @Description:
 * @date: 2019/7/2 0002 下午 15:05
 * @Version: V1.0
 */
@Service
public class SendService extends AbstractSendService {
    @Override
    public void handleConfirmCallback(String messageId, boolean ack, String cause) {

    }

    @Override
    public void handleReturnCallback(Message message, int replyCode, String replyText, String routingKey) {

    }
}
