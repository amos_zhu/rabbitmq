package com.amos.common.constants;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: ExchangeProperty
 * @Package: com.amos.common.constants
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 下午 15:18
 * @Version: V1.0
 */
public interface RabbitMQProperty {
    /**
     * 队列的属性
     */
    public interface QueueProperty {

    }

    /**
     * 交换机的属性
     */
    public interface ExchangeProperty {
        /**
         *
         */
        public static final String ALTERNATE_EXCHANGE = "alternate-exchange";

    }
}

