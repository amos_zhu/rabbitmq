package com.amos.common.constants;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: Constants
 * @Package: com.amos.common.constants
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 下午 15:59
 * @Version: V1.0
 */
public class Constants {
    /**
     * 默认参数不能为空的提示信息
     */
    public static final String PARAMETER_DEFAULT_NOT_NULL_EXCEPTION_MSG = "the parameter object could not be null";
}
