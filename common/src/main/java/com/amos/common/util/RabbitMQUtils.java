package com.amos.common.util;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: RabbitMQUtils
 * @Package: com.amos.common.util
 * @author: zhuqb
 * @Description:
 * @date: 2019/7/5 0005 下午 13:41
 * @Version: V1.0
 */
@Service
public class RabbitMQUtils {
    @Value("${rabbitmq.api-baseUrl:http://192.168.56.105:15672/api/}")
    private String apiBaseUrl;

    public static final String LIST_EXCHANGES = "exchanges";

    public List<Map<String, Object>> listExchanges() {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(this.apiBaseUrl + LIST_EXCHANGES).get().build();
        Call call = okHttpClient.newCall(request);
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (!StringUtils.isEmpty(request)) {
                response.close();
            }
        }
        return null;
    }
}
