package com.amos.common.util;

import java.util.UUID;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.util
 * @ClassName UUIDUtils
 * @Description uuid 操作类
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 19:07
 * @Version 1.0
 **/
public class UUIDUtils {
    /**
     * 生成随机的uuid
     *
     * @return
     */
    public static String generateUuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
