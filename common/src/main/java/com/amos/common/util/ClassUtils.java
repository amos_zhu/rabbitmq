package com.amos.common.util;

import com.amos.common.exception.RabbitMQExceptionUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: ClassUtils
 * @Package: com.amos.common.util
 * @author: amos
 * @Description: 操作类的工具方法
 * @date: 2019/6/28 0028 下午 15:49
 * @Version: V1.0
 */
public class ClassUtils {
    /**
     * 获取类中所有的field
     *
     * @param clazz
     * @return
     */
    public static List<Field> getAllField(Class clazz) {
        if (StringUtils.isEmpty(clazz)) {
            RabbitMQExceptionUtils.throwRabbitMQException();
        }
        Field[] fields = FieldUtils.getAllFields(clazz);
        if (fields != null && fields.length > 0) {
            return Arrays.asList(fields);
        }
        return new ArrayList<>();
    }
}
