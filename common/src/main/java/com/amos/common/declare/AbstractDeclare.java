package com.amos.common.declare;

import com.amos.common.bean.Result;
import com.amos.common.exception.RabbitMQExceptionUtils;
import com.amos.common.type.ResultEnum;
import com.amos.common.util.ValidateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Copyright (C)  五月软件工作室
 *
 * @Package com.amos.common.declare
 * @ClassName AbstractDeclare
 * @Description TODO
 * @Author Amos
 * @Modifier
 * @Date 2019/6/29 20:28
 * @Version 1.0
 **/
public abstract class AbstractDeclare {

    public final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    RabbitAdmin rabbitAdmin;

    /**
     * 自定义的校验
     *
     * @param object
     */
    public abstract void DefinedValidate(Object object);

    /**
     * 通用校验
     * 1. 校验字段是否是非空
     *
     * @param object
     */
    public void validate(Object object) {
        Result result = ValidateUtils.validate(object);
        if (!ResultEnum.success().equals(result.getCode())) {
            RabbitMQExceptionUtils.throwRabbitMQException(result.getMsg());
        }

        this.DefinedValidate(object);
    }
}
