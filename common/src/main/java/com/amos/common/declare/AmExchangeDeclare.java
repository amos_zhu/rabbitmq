package com.amos.common.declare;

import com.amos.common.bean.MqExchange;
import com.amos.common.exception.RabbitMQException;
import com.amos.common.type.ExchangeTypeEnum;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: AmExchangeDeclare
 * @Package: com.amos.common.declare
 * @author: amos
 * @Description: 交换机声明
 * @date: 2019/6/28 0028 下午 14:10
 * @Version: V1.0
 */
@Component
public class AmExchangeDeclare extends AbstractDeclare {

    @Autowired
    RabbitAdmin rabbitAdmin;

    /**
     * 向rabbitMQ服务器注册指定的交换机以及交换机的类型
     *
     * @param mqExchage
     * @return
     */
    public Exchange declareExchange(MqExchange mqExchage) {
        this.logger.info("declare exchange is :" + mqExchage.toString());

        Exchange exchange = null;

        super.validate(mqExchage);
        exchange = this.initExchange(mqExchage);
        this.rabbitAdmin.declareExchange(exchange);

        this.logger.info("declare exchange success");
        return exchange;
    }


    /**
     * 从RabbitMQ服务端上删除指定的交换机
     *
     * @param exchangeName
     * @return
     */
    public boolean deleteExchange(String exchangeName) {
        this.logger.info("delete exchange is : " + exchangeName);

        if (StringUtils.isEmpty(exchangeName)) {
            throw new RabbitMQException("the parameter exchangeName couldn't not be null");
        }

        return this.rabbitAdmin.deleteExchange(exchangeName);
    }

    /**
     * 根据不同类型初始化不同类型的交换机
     *
     * @param mqExchage
     * @return
     */
    private Exchange initExchange(MqExchange mqExchage) {
        ExchangeTypeEnum exchangeTypeEnum = mqExchage.getType();
        switch (exchangeTypeEnum) {
            case DIRECT:
                return new DirectExchange(mqExchage.getName(), mqExchage.isDurable(), mqExchage.isAutoDelete(), mqExchage.getArguments());
            case TOPIC:
                return new TopicExchange(mqExchage.getName(), mqExchage.isDurable(), mqExchage.isAutoDelete(), mqExchage.getArguments());
            case FANOUT:
                return new FanoutExchange(mqExchage.getName(), mqExchage.isDurable(), mqExchage.isAutoDelete(), mqExchage.getArguments());
            case HEADERS:
                return new HeadersExchange(mqExchage.getName(), mqExchage.isDurable(), mqExchage.isAutoDelete(), mqExchage.getArguments());
            default:
                return null;
        }
    }

    /**
     * 自定义校验规则
     *
     * @param object
     */
    @Override
    public void DefinedValidate(Object object) {

    }
}
