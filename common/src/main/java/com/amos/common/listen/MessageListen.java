package com.amos.common.listen;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.listen
 * @ClassName AbstractMessageHandle
 * @Description 队列设置监听基类
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 20:21
 * @Version 1.0
 **/
@Component
public class MessageListen {

    public final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ConnectionFactory connectionFactory;

    /**
     * 在容器中加入消息监听
     *
     * @param queue
     * @param messageHandler
     * @param isAck
     * @throws Exception
     */
    public void addMessageLister(String queue, AbstractMessageHandler messageHandler, boolean isAck) throws Exception {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(this.connectionFactory);
        container.setQueueNames(queue);
        AcknowledgeMode ack = AcknowledgeMode.NONE;
        if (isAck) {
            ack = AcknowledgeMode.MANUAL;
        }
        messageHandler.setAck(queue, ack);
        container.setAcknowledgeMode(ack);
        MessageListenerAdapter adapter = new MessageListenerAdapter(messageHandler);
        container.setMessageListener(adapter);
        container.start();
        this.logger.info("------ 已成功监听异步消息触发通知队列：" + queue + " ------");
    }
}
