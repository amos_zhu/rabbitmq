package com.amos.common.type;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: ExchangeTypeEnum
 * @Package: com.amos.common.type
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 下午 14:42
 * @Version: V1.0
 */
public enum ExchangeTypeEnum {
    /**
     * direct
     */
    DIRECT("direct"),
    /**
     * topic
     */
    TOPIC("topic"),
    /**
     * fanout
     */
    FANOUT("fanout"),
    /**
     * headers
     */
    HEADERS("headers");

    private String code;

    private ExchangeTypeEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
