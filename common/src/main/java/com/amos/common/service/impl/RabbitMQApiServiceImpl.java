package com.amos.common.service.impl;

import com.amos.common.service.RabbitMQApiService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: RabbitMQApiServiceImpl
 * @Package: com.amos.common.service.impl
 * @author: zhuqb
 * @Description:
 * @date: 2019/7/5 0005 下午 12:01
 * @Version: V1.0
 */
@Service
public class RabbitMQApiServiceImpl implements RabbitMQApiService {

    public List<Map<String, Object>> listExchanges() {
        return null;
    }
}
