package com.amos.common.bean;

import com.amos.common.type.ExchangeTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: MqExchange
 * @Package: com.amos.common.bean
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 下午 15:23
 * @Version: V1.0
 */
@Data
public class MqExchange {
    /**
     * 默认的DIRECT类型的交换机
     */
    public static final String DEFAULT_DIRECT_EXCHANGE = "amq.direct";
    /**
     * 默认的TOPIC类型的交换机
     */
    public static final String DEFAULT_TOPIC_EXCHANGE = "amq.topic";
    /**
     * 默认的HEADERS类型的交换机
     */
    public static final String DEFAULT_HEADERS_EXCHANGE = "amq.headers";
    /**
     * 默认的FANOUT类型的交换机
     */
    public static final String DEFAULT_FANOUT_EXCHANGE = "amq.fanout";
    /**
     * 交换机的名称
     */
    @NotNull(message = "交换机名称不能为空")
    private String name;

    /**
     * 交换机的类型
     */
    @NotNull(message = "交换机类型不能为空")
    private ExchangeTypeEnum type;
    /**
     * 是否持久化
     * 持久化可以将交换机存盘，在服务器重启的时候不会丢失相关的信息
     * 默认是开启持久化
     */
    private boolean durable = Boolean.TRUE;
    /**
     * 是否自动删除
     * 自动删除的前提是至少有一个队列或者交换机与这个交互机绑定，之后所有与这个交换机绑定的队列或者交换机都与此解绑
     */
    private boolean autoDelete;

    public MqExchange name(String name) {
        this.name = name;
        return this;
    }


    public MqExchange type(ExchangeTypeEnum type) {
        this.type = type;
        return this;
    }

    public MqExchange durable(boolean durable) {
        this.durable = durable;
        return this;
    }

    public MqExchange autoDelete(boolean autoDelete) {
        this.autoDelete = autoDelete;
        return this;
    }

    public MqExchange arguments(Map<String, Object> arguments) {
        this.arguments = arguments;
        return this;
    }

    /**
     * 自定义属性参数
     * 比如：alternate-exchange
     */
    private Map<String, Object> arguments;


}
