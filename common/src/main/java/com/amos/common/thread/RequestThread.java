package com.amos.common.thread;

import com.amos.common.request.Request;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: RequestThread
 * @Package: com.amos.common.thread
 * @author: zhuqb
 * @Description: 执行请求的工作线程
 * <p/>
 * 线程和队列进行绑定，然后再线程中处理对应的业务逻辑
 * @date: 2019/7/15 0015 下午 14:34
 * @Version: V1.0
 */
public class RequestThread implements Callable<Boolean> {
    /**
     * 队列
     */
    private ArrayBlockingQueue<Request> queue;

    public RequestThread(ArrayBlockingQueue<Request> queue) {
        this.queue = queue;
    }

    /**
     * 方法中执行具体的业务逻辑
     * TODO
     *
     * @return
     * @throws Exception
     */
    @Override
    public Boolean call() throws Exception {
        return true;
    }
}
