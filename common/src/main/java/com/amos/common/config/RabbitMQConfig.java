package com.amos.common.config;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: RabbitMQConfig
 * @Package: com.amos.producer.config
 * @author: amos
 * @Description: rabbitMQ的配置
 * @date: 2019/6/28 0028 上午 9:11
 * @Version: V1.0
 */
@Configuration
public class RabbitMQConfig {
    /**
     * rabbitMQ服务器的地址
     */
    @Value("${spring.rabbitmq.addresses:192.168.56.105:5672}")
    private String addresses;
    /**
     * rabbitMQ用户名
     */
    @Value("${spring.rabbitmq.username:root}")
    private String username;
    /**
     * rabbitMQ密码
     */
    @Value("${spring.rabbitmq.password:root}")
    private String password;
    /**
     * rabbitMQ虚拟机 这里默认 /
     */
    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;
    /**
     * 消息发送失败，是否回调给发送者
     */
    @Value("${spring.rabbitmq.template.mandatory:false}")
    private Boolean mandatory;
    /**
     * 是否确认
     */
    @Value("${spring.rabbitmq.publisher-confirms:false}")
    private Boolean publisherConfirms;
    /**
     * 如果mandatorys设置成true，该值也设置 成true
     */
    @Value("${spring.rabbitmq.publisher-returns:false}")
    private Boolean publisherReturns;

    /**
     * 注册rabbitMQ的Connection
     *
     * @return
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setAddresses(this.addresses);
        cachingConnectionFactory.setUsername(this.username);
        cachingConnectionFactory.setPassword(this.password);
        cachingConnectionFactory.setVirtualHost(this.virtualHost);
        // 如果消息要设置成回调，则以下的配置必须要设置成true
        cachingConnectionFactory.setPublisherConfirms(this.publisherConfirms);
        cachingConnectionFactory.setPublisherReturns(this.publisherReturns);
        return cachingConnectionFactory;
    }

    /**
     * 因为要设置回调类，所以应是prototype类型，如果是singleton类型，则回调类为最后一次设置
     * 主要是为了设置回调类
     *
     * @return
     */
    @Bean(name = "rabbitTemplate")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(this.connectionFactory());
        template.setMessageConverter(new Jackson2JsonMessageConverter());
        return template;
    }

    /**
     * 注册rabbitAdmin 方便管理
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }
}
