package com.amos.common.message;

import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Package com.amos.common.message
 * @ClassName MqMessage
 * @Description 消息的封装类
 * @Author Amos
 * @Modifier
 * @Date 2019/7/1 15:15
 * @Version 1.0
 **/
@Data
public class MqMessage {
    /**
     * 交换机名称
     */
    private String exchangeName;
    /**
     * 队列名称
     */
    private String queueName;
    /**
     * 消息Id
     */
    private String messageId;
    /**
     * 消息体
     */
    private Object messageBody;
    /**
     * 路由键
     */
    private String routingKey;
    /**
     * 消息优先级
     */
    private String messagePriority;
    /**
     * 补偿次数
     */
    private Integer tryNum;


}
