package com.amos.common.message;

import lombok.Data;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: Message
 * @Package: com.amos.common.message
 * @author: zhuqb
 * @Description: 发送和接收的消息
 * @date: 2019/7/2 0002 下午 17:38
 * @Version: V1.0
 */
@Data
public class MessageData {
    /**
     * 消息的内容
     */
    private Object content;
    /**
     * 发送方
     */
    private String sender;
    /**
     * 接收方
     */
    private String receiver;
    /**
     * 消息的类型（发布的内容/指令）
     */
    private String msgType;
    /**
     * 唯一标志
     */
    private String tag;
    /**
     * 处理结果
     */
    private boolean handleResult;
}
