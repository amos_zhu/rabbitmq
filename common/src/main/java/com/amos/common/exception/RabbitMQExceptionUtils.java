package com.amos.common.exception;

import com.amos.common.constants.Constants;

/**
 * Copyright (C)  五月软件工作室
 *
 * @Package com.amos.common.exception
 * @ClassName RabbitMQExceptionUtils
 * @Description 异常日志操作工具类
 * @Author Amos
 * @Modifier
 * @Date 2019/6/29 20:53
 * @Version 1.0
 **/
public class RabbitMQExceptionUtils {
    /**
     * 实例化RabbitMQException的异常
     *
     * @param errMsg
     * @return
     */
    public static RabbitMQException throwRabbitMQException(String errMsg) {
        return new RabbitMQException(errMsg);
    }

    /**
     * 实例化默认的RabbitMQException的异常
     *
     * @return
     */
    public static RabbitMQException throwRabbitMQException() {
        return new RabbitMQException(Constants.PARAMETER_DEFAULT_NOT_NULL_EXCEPTION_MSG);
    }
}
