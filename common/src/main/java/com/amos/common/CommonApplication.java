package com.amos.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright © 2018 五月工作室. All rights reserved.
 *
 * @Project: rabbitmq
 * @ClassName: CommonApplication
 * @Package: com.amos.consumer
 * @author: amos
 * @Description:
 * @date: 2019/6/28 0028 上午 11:50
 * @Version: V1.0
 */
@SpringBootApplication(scanBasePackages = {"com.amos.common.thread"})
public class CommonApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonApplication.class, args);
    }
}
